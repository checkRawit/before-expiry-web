import { db } from "./firebase";

// Collection Reference
const items = db.collection("items");

export const getItem = async (itemId) => {
  const docSnapshot = await items.doc(itemId).get();
  const docData = docSnapshot.data();
  if (docData === undefined) throw Error("Item not found");
  return {
    id: docSnapshot.id,
    name: docData.name,
    expirationDate: docData.expirationDate.toDate(),
    isUsed: docData.isUsed,
  };
};

export const createItem = async (name, expirationDate) => {
  const data = {
    name,
    expirationDate,
    isUsed: false,
  };
  await items.add(data);
};

export const updateItemIsUsed = async (itemId, isUsed) => {
  const doc = items.doc(itemId);
  const docSnapshot = await doc.get();
  if (docSnapshot.get("expirationDate").toDate() <= new Date())
    throw new Error(
      "Cannot set item isUsed field because expiration date is pass"
    );
  await doc.update({ isUsed });
};

export const updateItem = async (itemId, data) => {
  await items.doc(itemId).update(data);
};

export const deleteItem = async (itemId) => {
  await items.doc(itemId).delete();
};

export function listenItemsSnapshot(callback) {
  const unsubscribeFunction = db
    .collection("items")
    .orderBy("isUsed", "asc")
    .orderBy("expirationDate", "asc")
    .onSnapshot((querySnapshot) => {
      callback(
        querySnapshot.docs.map((doc) => ({
          id: doc.id,
          name: doc.get("name"),
          expirationDate: doc.get("expirationDate").toDate(),
          isUsed: doc.get("isUsed"),
        }))
      );
    });
  return unsubscribeFunction;
}
