import firebase from './firebase'

export const signIn = async (email, password) => {
    await firebase.auth().signInWithEmailAndPassword(email, password)
}

export const signOut = () => {
    firebase.auth().signOut()
}