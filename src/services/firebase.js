import * as firebase from 'firebase/app'

// TODO: Add SDKs for Firebase products that you want to use
//       https://firebase.google.com/docs/web/setup#available-libraries
import 'firebase/analytics'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyCSDa8WEJ9ROYEMze8fG8OTGvzSbLcRdmU",
  authDomain: "before-expiry.firebaseapp.com",
  databaseURL: "https://before-expiry.firebaseio.com",
  projectId: "before-expiry",
  storageBucket: "before-expiry.appspot.com",
  messagingSenderId: "987594904502",
  appId: "1:987594904502:web:43351c74a82954b139e717",
  measurementId: "G-CZP6TSG0LV",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

export const db = firebase.firestore();
export default firebase