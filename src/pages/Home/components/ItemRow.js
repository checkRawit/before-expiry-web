import React, { useCallback } from "react";
import classNames from "classnames";
import { Link } from "react-router-dom";

const ITEM_STATUS = {
  NORMAL: "NORMAL",
  WARNING: "WARNING",
  CAUTION: "CAUTION",
  BAD: "BAD",
};

const dayToMilli = (day) => day * 24 * 60 * 60 * 1000;

export default (props) => {
  const { id, name, isUsed, expirationDate, onChangeCheckbox } = props;
  const handleCheckboxEvent = useCallback(
    (event) => {
      onChangeCheckbox(event.target.value, event.target.checked);
    },
    [onChangeCheckbox]
  );

  const diffDate = expirationDate - new Date();
  const itemStatus = isUsed
    ? ITEM_STATUS.NORMAL
    : diffDate <= dayToMilli(0)
    ? ITEM_STATUS.BAD
    : diffDate <= dayToMilli(3)
    ? ITEM_STATUS.CAUTION
    : diffDate <= dayToMilli(7)
    ? ITEM_STATUS.WARNING
    : ITEM_STATUS.NORMAL;

  return (
    <tr
      className={classNames({
        "has-text-danger": itemStatus === ITEM_STATUS.BAD,
        "has-text-caution": itemStatus === ITEM_STATUS.CAUTION,
        "has-text-warning": itemStatus === ITEM_STATUS.WARNING,
      })}
    >
      <td>
        <input
          type="checkbox"
          checked={isUsed}
          value={id}
          onChange={handleCheckboxEvent}
        />
      </td>
      <td>
        <Link to={`/items/${id}`}>{name}</Link>
      </td>
      <td>{expirationDate.toDateString()}</td>
    </tr>
  );
};
