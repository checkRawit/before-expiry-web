import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";

import {
  listenItemsSnapshot,
  updateItemIsUsed,
} from "../../services/items";
import { StoreContext } from "../../contexts/store";
import ItemRow from "./components/ItemRow";

function HomePage(props) {
  const [items, setItems] = useState([]);
  const { state: storeState } = useContext(StoreContext);

  /* listen to user status */
  useEffect(() => {
    if (storeState.user) {
      // if user is signed in
      // add listener for setItems
      return listenItemsSnapshot(setItems);
    } else {
      // if user is not signed in
      // set items to empty
      setItems([]);
    }
  }, [storeState.user]);

  return (
    <section className="section">
      <div className="container is-small">
        {/* Heading */}
        <h5 className="title">Item List</h5>
        <hr />

        {/* table */}
        <table className="table is-fullwidth">
          <thead>
            <tr>
              <th>Use</th>
              <th>Name</th>
              <th>Exp. date</th>
            </tr>
          </thead>
          <tbody>
            {items.map((item) => (
              <ItemRow
                key={item.id}
                id={item.id}
                name={item.name}
                isUsed={item.isUsed}
                expirationDate={item.expirationDate}
                onChangeCheckbox={updateItemIsUsed}
              />
            ))}
          </tbody>
        </table>

        {/* Link to AddItem Page */}
        <Link to="/add">
          <button className="button is-primary">Add</button>
        </Link>
      </div>
    </section>
  );
}

export default HomePage;
