import React, { useEffect, useState } from "react";

import ItemForm from "../../components/ItemForm";
import { updateItem, getItem, deleteItem } from "../../services/items";
import { useParams, useHistory } from "react-router-dom";

const INITIAL_ITEM = {
  id: undefined,
  name: '',
  expirationDate: undefined,
  isUsed: false,
}

export default (props) => {
  const { itemId } = useParams()
  const history = useHistory();

  const [item, setItem] = useState(INITIAL_ITEM)

  useEffect(() => {
    (async () => {
      if (!itemId) return
      try {
        setItem(await getItem(itemId))
      } catch (error) {
        console.error(error)
        setItem(INITIAL_ITEM)
      }
    })()
  }, [itemId])

  const onSubmit = async (name, expirationDate, isUsed) => {
    await updateItem(itemId, { name, expirationDate, isUsed })
    history.push('/')
  }

  const onDelete = async () => {
    await deleteItem(itemId)
    history.push('/')
  }

  return (
    <section className="section">
      <div className="container is-small">
        <h5 className="title">Update Item</h5>
        <hr />
        <ItemForm onSubmit={onSubmit} onDelete={onDelete} onCancel={() => history.push('/')} defaultItem={item} />
      </div>
    </section>
  );
};
