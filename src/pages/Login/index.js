import React, { useState, useCallback } from "react";
import { useHistory } from "react-router-dom";
import classNames from "classnames";

import { signIn } from "../../services/auth";

const LoginPage = (props) => {
  // react-router
  const history = useHistory();

  // UI state
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoginError, setIsLoginError] = useState(false);
  const [isLogginIn, setIsLogginIn] = useState(false);

  const handleEmailInputChangeEvent = useCallback(
    (event) => setEmail(event.target.value),
    [setEmail]
  );

  const handlePasswordInputChangeEvent = useCallback(
    (event) => setPassword(event.target.value),
    [setPassword]
  );

  const handleFormSubmitEvent = useCallback(
    async (event) => {
      event.preventDefault();
      setIsLogginIn(true);
      try {
        await signIn(email, password);
        // go to home page
        history.push("/");
        setIsLoginError(false);
      } catch (error) {
        setIsLoginError({ message: "Sign in error, please try again" });
      }
      setIsLogginIn(false);
    },
    [email, password, history]
  );

  return (
    <section className="section">
      <div className="container is-small">
        <h5 className="title">Login</h5>
        <hr />

        {/* Notification */}
        <div
          className={classNames({
            notification: true,
            "is-info": true,
            "is-danger": isLoginError,
          })}
        >
          {isLoginError.message || "Please sign in with an email and password."}
        </div>

        {/* Form */}
        <form action="POST" onSubmit={handleFormSubmitEvent}>
          <fieldset disabled={isLogginIn}>
            {/* Email Input */}
            <div className="field">
              <label className="label">Email</label>
              <div className="control">
                <input
                  type="text"
                  className="input"
                  value={email}
                  onChange={handleEmailInputChangeEvent}
                />
              </div>
            </div>

            {/* Password Input */}
            <div className="field">
              <label className="label">Password</label>
              <div className="control">
                <input
                  type="password"
                  className="input"
                  value={password}
                  onChange={handlePasswordInputChangeEvent}
                />
              </div>
            </div>

            {/* Button */}
            <div className="field">
              <div className="control">
                <button
                  className={classNames({
                    button: true,
                    "is-success": true,
                    "is-loading": isLogginIn,
                  })}
                  type="submit"
                >
                  Sign In
                </button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </section>
  );
};

export default LoginPage;
