import React from "react";

import ItemForm from "../../components/ItemForm";
import { createItem } from "../../services/items";
import { useHistory } from "react-router-dom";

const INITIAL_ITEM = {
  id: undefined,
  name: '',
  expirationDate: undefined,
  isUsed: false,
}

export default (props) => {
  const history = useHistory();

  const onSubmit = async (name, expirationDate, isUsed) => {
    await createItem(name, expirationDate, isUsed)
    history.push('/')
  }

  const onCancel = () => {
    history.push('/')
  }

  return (
    <section className="section">
      <div className="container is-small">
        <h5 className="title">Add Item</h5>
        <hr />
        <ItemForm onSubmit={onSubmit} onCancel={onCancel} defaultItem={INITIAL_ITEM} />
      </div>
    </section>
  );
};
