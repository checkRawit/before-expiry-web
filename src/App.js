import React, { useEffect, useContext } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { StoreContext } from "./contexts/store";
import firebase from "./services/firebase";
import HomePage from "./pages/Home";
import LoginPage from "./pages/Login";
import AddItemPage from "./pages/AddItem";
import UpdateItemPage from "./pages/UpdateItem";
import Header from "./components/Header";
import "./App.scss";

function App() {
  const { dispatch: storeDispatch } = useContext(StoreContext);

  // integrate firebase auth
  useEffect(() => {
    return firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        storeDispatch({
          type: "USER_LOGIN",
          payload: {
            uid: user.uid,
          },
        });
      } else {
        storeDispatch({ type: "USER_LOGOUT" });
      }
    });
  }, [storeDispatch]);

  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/login" children={<LoginPage />} />
        <Route path="/items/:itemId" children={<UpdateItemPage />} />
        <Route path="/add" children={<AddItemPage />} />
        <Route path="/" children={<HomePage />} />
      </Switch>
    </Router>
  );
}

export default App;
