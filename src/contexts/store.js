import React, { createContext, useReducer, useMemo } from 'react'

const INITIAL_STATE = {
    user: undefined
}

const StoreContext = createContext({ state: INITIAL_STATE, dispatch: () => null })

const userReducer = (state, action) => {
    switch (action.type) {
        case 'USER_LOGIN':
            return { ...state, user: action.payload.uid }
        case 'USER_LOGOUT':
            return { ...state, user: null }
        default:
            throw new Error('Invalid action type in userReducer')
    }
}

const StoreProvider = ({ children }) => {
    const [state, dispatch] = useReducer(userReducer, INITIAL_STATE)

    const store = useMemo(() => ({ state, dispatch }), [state])
    return <StoreContext.Provider value={store}>
        {children}
    </StoreContext.Provider>
}

export { StoreContext, StoreProvider }
