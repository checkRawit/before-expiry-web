import React, { useContext } from "react";
import { StoreContext } from "../contexts/store";
import { signOut } from "../services/auth";
import { Link } from "react-router-dom";

export default (props) => {
  const { state: storeState } = useContext(StoreContext);

  return (
    <section className="hero is-primary">
      <div className="hero-body">
        <div className="container is-small">
          {/* Title */}
          <h1 className="title">
            Before Expiry
            <span role="img" aria-label="plate" className="ml-2">
              🍴
            </span>
          </h1>
          <h2 className="subtitle is-6">Manage your food before it expire</h2>
          {storeState.user ? (
            <button className="button is-danger" onClick={signOut}>
              Sign out
            </button>
          ) : (
            <Link to="/login">
              <button className="button">Sign in</button>
            </Link>
          )}
        </div>
      </div>
    </section>
  );
};
