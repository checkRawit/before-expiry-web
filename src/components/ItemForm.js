import React, { useState, useEffect } from "react";
import { format } from "date-fns";

const DATE_FORMAT = "yyyy-MM-dd";

function ItemForm(props) {
  const { defaultItem, onSubmit, onCancel, onDelete } = props;

  const [name, setName] = useState("");
  const [expirationDate, setExpirationDate] = useState();
  const [isUsed, setIsUsed] = useState(false);

  useEffect(() => {
    setName(defaultItem.name);
    setExpirationDate(defaultItem.expirationDate);
    setIsUsed(defaultItem.isUsed);
  }, [defaultItem]);

  const handleSubmit = async () => {
    const isValid = name !== "" && expirationDate !== undefined;
    isValid && onSubmit(name, expirationDate, isUsed);
  };

  const handleInputDateChange = (e) => {
    setExpirationDate(new Date(e.target.value));
  };

  return (
    <>
      <div className="field">
        <label className="label">Item Name</label>
        <div className="control">
          <input
            type="text"
            placeholder="Item name"
            className="input"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Expiration Date</label>
        <div className="control" style={{ minWidth: "18em" }}>
          <input
            type="date"
            value={expirationDate ? format(expirationDate, DATE_FORMAT) : ""}
            onChange={handleInputDateChange}
            className="input"
          />
        </div>
      </div>
      <div className="field is-grouped">
        {onSubmit &&
          <div className="control">
            <button className="button is-success" onClick={handleSubmit}>
              Submit
          </button>
          </div>
        }
        {onDelete &&
          <div className="control">
            <button className="button is-danger" onClick={onDelete}>
              Delete
            </button>
          </div>
        }
        {onCancel &&
          <div className="control">
            <button className="button" onClick={onCancel}>
              Cancel
          </button>
          </div>
        }
      </div>
    </>
    // <div className="columns">
    //   <div className="column">
    // <input
    //   type="text"
    //   placeholder="Item name"
    //   className="input"
    //   onChange={(e) => setName(e.target.value)}
    //   value={name}
    // />
    //   </div>
    //   <div className="column is-narrow" style={{ minWidth: "18em" }}>
    // <BulmaCalendar
    //   onSelect={setExpirationDate}
    //   color="danger"
    //   date={expirationDate}
    // />
    //   </div>
    //   <div className="column is-narrow">
    // <button
    //   className="button is-fullwidth is-primary"
    //   onClick={handleSubmit}
    // >
    //   Add
    // </button>
    //   </div>
    // </div>
  );
}

export default ItemForm;
